```
The skriptkontrol.sh script was quickly written to pimp a FVWM default
environment on OpenBSD. If you run it automatically at startup, the first
and only one "dialog box" displayed, will allow you to choose either your
cpu computes at high (default) performances, or at the lowest level the
APMD offers. I found this trivial change quite useful while using a laptop.
Clearly, it depends on what kind of processor or laptop you're using.

********************************* FVWMrc ***************************************

Note : I noticed a thin "edge effect" left behind an upgrade from a version
of OpenBSD to another : the title of the fvwm's Pager remains the same.
So, if you do have already customized it, you should have a $HOME/.fvmrc
(else, you can % cp /usr/X11R6/lib/X11/fvwm/.fvwmrc ~/.fvwmrc), in
which you can find a line similar to the following one :

*FvwmPagerLabel 0 "OpenBSD 5.6"

for example. It just needs to be corrected by hand, thus at the next start
of a fvwm session, the title of the Pager will match with your current
system's version.

If you wonder what my X environment looks like, you could check both the
screenshot attached at this git, and my .fvwmrc. In that one, I tried to
get a convenient behavior from FVWM, as well as to disable some annoying
(IMHO) default features.

********************************** Nedit ***************************************

Assuming tough hackers (or the relative complement of {those insane} in
{lads and lasses}) don't touch anything else than Vi(m) or Emacs (which I
used to handle for a while, even now occasionally), this part deals with those
who could be interested in setting Nedit up. I reckon this GUI editor seduced
me, mainly because of its simplicity, and the fact it was build on ``Motif''
libs. As far as it manages regex... and block selection !


The following lines are just a selection concerning Preferences from my
~/.nedit/nedit.rc :

nedit.smartIndentInitCommon: Default
! automatic wraping every 80 cols
nedit.autoWrap: Newline
nedit.wrapMargin: 80
nedit.showWrapMargin: Always

nedit.autoIndent: Auto
nedit.autoSave: True
nedit.openInTab: True
nedit.saveOldVersion: False
nedit.showMatching: Delimiter
nedit.matchSyntaxBased: True
nedit.highlightSyntax: True
nedit.backlightChars: False
nedit.searchDialogs: False
nedit.beepOnSearchWrap: False
nedit.retainSearchDialogs: False
nedit.searchWraps: True
nedit.stickyCaseSenseButton: True
nedit.repositionDialogs: True
nedit.autoScroll: False
nedit.appendLF: True
nedit.sortOpenPrevMenu: True
nedit.statisticsLine: True
nedit.iSearchLine: False
nedit.sortTabs: False
nedit.tabBar: True
nedit.tabBarHideOne: True
nedit.toolTips: True
nedit.globalTabNavigate: False
nedit.lineNumbers: True
nedit.pathInWindowsMenu: True
nedit.warnFileMods: True
nedit.warnRealFileMods: True
nedit.warnExit: True
nedit.searchMethod: Literal
! comfortable frame
nedit.textRows: 24
nedit.textCols: 82
nedit.tabDistance: 4
nedit.emulateTabs: 0
nedit.insertTabs: True
nedit.textFont: -*-courier-medium-r-normal--*-120-*-*-*-iso8859-1
nedit.boldHighlightFont: -*-courier-bold-r-normal--*-120-*-*-*-iso8859-1
nedit.italicHighlightFont: -*-courier-medium-o-normal--*-120-*-*-*-iso8859-1
nedit.boldItalicHighlightFont: -*-courier-bold-o-normal--*-120-*-*-*-iso8859-1
nedit.textFgColor: black
nedit.textBgColor: beige
nedit.selectFgColor: black
nedit.selectBgColor: rgb:cc/cc/cc
nedit.hiliteFgColor: red
nedit.hiliteBgColor: red
! nice line number
nedit.lineNoFgColor: darkcyan
nedit.cursorFgColor: black
nedit.wrapMarginForeground: black
nedit.smartTags: True
nedit.prefFileRead: True
! to identify it easily in FvwmWinList
nedit.titleFormat: Nedit - {%c} [%s] %f (%S) - %d


--

My P^HKorn shell PS1 is the next one :

PROMPITO="\u##\$:\h:\w:\n>>"
PS1="$(printf "\033[0;36m%s\033[0;37m  " $PROMPITO)"

# to be added within $HOME/.kshrc, obviously.
```
