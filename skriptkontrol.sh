#!/bin/sh
xterm -geometry 94x46+514+0 -e "clear;figlet -f \
	speed $(echo "$(uname -s -r)" | tr '[:lower:]' '[:upper:]');
figlet -f graffiti $USER;cat ~/.puffy;ksh" &
xset b off &
xrdb $HOME/.Xdefaults &

# ===============to=be=commented=if=fvwmrc=left=unchanged===================== #

xclock -bg grey -fg black -hd black -hl black -padding 0 -update 1 \
	-geometry 150x150+1116+395 &
xload -bg grey -geometry +1116+559 &

# ===============================performances================================= #

echo " __________________________________________________________________"
echo " | 1) minimal cpu freq, requires root passwd (else != 1)          |"
echo " | 2) start default apm daemon...                                 |"
echo " ------------------------------------------------------------------"
read choix_1
case $choix_1 in
    1) (su -l root -c "apmd; apm -L")
    ;;
    2) (apmd)
esac

apmd

# ============================================================================ #

i="0"
while [ $i -lt 1 ]
do

clear

# ==========================huge==linear==goret==part========================= #
echo " "
echo -n "        " && uname -srp && echo -n "  *** CHECKING SYSTEM TERM ***" \
	&& echo "\n" && apm && \
	echo "----------------------------------------------------------" && \
	df -h && echo "----------------------------------------------------------" \
	&& sysctl | grep sensors.cpu && \
	echo "----------------------------------------------------------" && \
	top | head -14 | tail -8 | awk '{$1=$2=$3=$4=""; print $0}' && \
	echo "\n\t\t\t\t      [by serguei.chmod77]" && \
	echo "----------------------------------------------------------" && \
	figlet -f graffiti Kontrol &

# both of the (n,n') values in "head -14 | tail -8" before awk's treatment are
# relative to the number of your cpu threads. So it's up to you to adjust these
# integers to get the appropriate selection.

pid=$!
sleep 2
kill $pid
done
